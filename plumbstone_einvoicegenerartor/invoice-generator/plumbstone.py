# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'plumbstone.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
import datetime
from num2words import num2words


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(980, 540)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")




        #saving a filename and invoice details
        self.label_name = QtWidgets.QLabel(self.centralwidget)
        self.label_name.setGeometry(QtCore.QRect(10, 10, 80, 17))
        self.label_name.setObjectName("label_name")

        self.label_address = QtWidgets.QLabel(self.centralwidget)
        self.label_address.setGeometry(QtCore.QRect(10, 50, 80, 17))
        self.label_address.setObjectName("label_address")

        self.label_byrsgst = QtWidgets.QLabel(self.centralwidget)
        self.label_byrsgst.setGeometry(QtCore.QRect(10, 90, 100, 17))
        self.label_byrsgst.setObjectName("label_byrsgst")

        self.label_invoice = QtWidgets.QLabel(self.centralwidget)
        self.label_invoice.setGeometry(QtCore.QRect(420, 10 ,100, 17))
        self.label_invoice.setObjectName("label_invoice")

        self.label_challan = QtWidgets.QLabel(self.centralwidget)
        self.label_challan.setGeometry(QtCore.QRect(420, 50, 100, 17))
        self.label_challan.setObjectName("label_challan")

        self.label_product = QtWidgets.QLabel(self.centralwidget)
        self.label_product.setGeometry(QtCore.QRect(10, 130, 80, 17))
        self.label_product.setObjectName("label_product")

        self.label_hsn = QtWidgets.QLabel(self.centralwidget)
        self.label_hsn.setGeometry(QtCore.QRect(400, 130, 80, 17))
        self.label_hsn.setObjectName("label_hsn")

        self.label_Quantity = QtWidgets.QLabel(self.centralwidget)
        self.label_Quantity.setGeometry(QtCore.QRect(550, 130, 80, 17))
        self.label_Quantity.setObjectName("label_Quantity")

        self.label_measure = QtWidgets.QLabel(self.centralwidget)
        self.label_measure.setGeometry(QtCore.QRect(680, 130, 80, 17))
        self.label_measure.setObjectName("label_measure")

        self.label_price = QtWidgets.QLabel(self.centralwidget)
        self.label_price.setGeometry(QtCore.QRect(810, 130, 80, 17))
        self.label_price.setObjectName("label_price")

        self.label_date = QtWidgets.QLabel(self.centralwidget)
        self.label_date.setGeometry(QtCore.QRect(420, 90, 80, 17))
        self.label_date.setObjectName("label_date")

        """-------------textboxes-----------------------------------"""

        self.name_textbox = QtWidgets.QLineEdit(self.centralwidget)
        self.name_textbox.setGeometry(QtCore.QRect(110, 10, 150, 30))
        self.name_textbox.setObjectName("name_textbox")

        self.address_textbox = QtWidgets.QLineEdit(self.centralwidget)
        self.address_textbox.setGeometry(QtCore.QRect(110, 50, 300, 30))
        self.address_textbox.setObjectName("address_textbox")

        self.byrsgst_gst_textbox = QtWidgets.QLineEdit(self.centralwidget)
        self.byrsgst_gst_textbox.setGeometry(QtCore.QRect(110, 90, 150, 30))
        self.byrsgst_gst_textbox.setObjectName("byrsgst_gst_textbox")

        self.invoice_textbox = QtWidgets.QLineEdit(self.centralwidget)
        self.invoice_textbox.setGeometry(QtCore.QRect(520, 10, 100, 30))
        self.invoice_textbox.setObjectName("invoice_textbox")

        self.challan_textbox = QtWidgets.QLineEdit(self.centralwidget)
        self.challan_textbox.setGeometry(QtCore.QRect(520, 50, 100, 30))
        self.challan_textbox.setObjectName("challan_textbox")

        self.date_textbox = QtWidgets.QLineEdit(self.centralwidget)
        self.date_textbox.setGeometry(QtCore.QRect(520, 90, 100, 30))
        self.date_textbox.setObjectName("date_textbox")

        self.product_textbox = QtWidgets.QLineEdit(self.centralwidget)
        self.product_textbox.setGeometry(QtCore.QRect(110, 130, 280, 30))
        self.product_textbox.setObjectName("product_textbox")

        self.hsn_textbox = QtWidgets.QLineEdit(self.centralwidget)
        self.hsn_textbox.setGeometry(QtCore.QRect(440, 130, 100, 30))
        self.hsn_textbox.setObjectName("hsn_textbox")

        self.quantity_textbox = QtWidgets.QLineEdit(self.centralwidget)
        self.quantity_textbox.setGeometry(QtCore.QRect(620, 130, 50, 30))
        self.quantity_textbox.setObjectName("quantity_textbox")

        self.measure_textbox = QtWidgets.QLineEdit(self.centralwidget)
        self.measure_textbox.setGeometry(QtCore.QRect(750, 130, 50, 30))
        self.measure_textbox.setObjectName("measure_textbox")

        self.price_textbox = QtWidgets.QLineEdit(self.centralwidget)
        self.price_textbox.setGeometry(QtCore.QRect(850, 130, 100, 30))
        self.price_textbox.setObjectName("price_textbox")


        """----------------------------------BUTTONS-------------------------------------"""
        self.additem_Button = QtWidgets.QPushButton(self.centralwidget)
        self.additem_Button.setGeometry(QtCore.QRect(720, 240, 89, 25))
        self.additem_Button.setObjectName("additem_Button")
        self.additem_Button.clicked.connect(self.addRow)

        self.finalbill_Button = QtWidgets.QPushButton(self.centralwidget)
        self.finalbill_Button.setGeometry(QtCore.QRect(820, 300, 91, 25))
        self.finalbill_Button.setObjectName("finalbill_Button")
        self.finalbill_Button.clicked.connect(self.addNewPasses)

        self.print_Button = QtWidgets.QPushButton(self.centralwidget)
        self.print_Button.setGeometry(QtCore.QRect(820, 240, 89, 25))
        self.print_Button.setObjectName("print_button")
        self.print_Button.clicked.connect(self.printdoc)

        self.save_Button = QtWidgets.QPushButton(self.centralwidget)
        self.save_Button.setGeometry(QtCore.QRect(720, 300, 89, 25))
        self.save_Button.setObjectName("save_Button")
        self.save_Button.setEnabled(False)
        self.save_Button.clicked.connect(self.file_save)


        self.igst_checkbox = QtWidgets.QCheckBox(self.centralwidget)
        self.igst_checkbox.setGeometry(QtCore.QRect(700, 20, 112, 23))
        self.igst_checkbox.setObjectName("igst_checkbox")


        self.gst12_checkbox= QtWidgets.QCheckBox(self.centralwidget)
        self.gst12_checkbox.setGeometry(QtCore.QRect(700, 60, 112, 23))
        self.gst12_checkbox.setObjectName("gst12_checkbox")

        self.gst5_checkbox= QtWidgets.QCheckBox(self.centralwidget)
        self.gst5_checkbox.setGeometry(QtCore.QRect(700, 100, 112, 23))
        self.gst5_checkbox.setObjectName("gst5_checkbox")


        """----------------------table---------------------------------------"""


        self.tableWidget = QtWidgets.QTableWidget(self.centralwidget)
        self.tableWidget.setGeometry(QtCore.QRect(20, 180, 600, 300))
        self.tableWidget.setAutoFillBackground(False)
        self.tableWidget.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.tableWidget.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.tableWidget.setLineWidth(1)
        self.tableWidget.setSizeAdjustPolicy(QtWidgets.QAbstractScrollArea.AdjustToContents)
        self.tableWidget.setRowCount(0)
        self.tableWidget.setColumnCount(5)
        self.tableWidget.setObjectName("tableWidget")
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(2, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(3, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(4, item)
        #self.tableWidget.clicked.connect()


        """------------------menubar----------------------"""

        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 943, 22))
        self.menubar.setObjectName("menubar")
        self.menu_file = QtWidgets.QMenu(self.menubar)
        self.menu_file.setObjectName("menu_file")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)


        self.actionnew_invoice = QtWidgets.QAction(MainWindow)
        self.actionnew_invoice.setObjectName("actionnew_invoice")
        self.actionnew_invoice.triggered.connect(self.new_invoice)

        self.actionopen_invoice = QtWidgets.QAction(MainWindow)
        self.actionopen_invoice.setObjectName("actionopen_invoice")
        self.actionsave = QtWidgets.QAction(MainWindow)
        self.actionsave.setObjectName("actionsave")
        self.actionsave.triggered.connect(self.file_save)

        self.actionexit = QtWidgets.QAction(MainWindow)
        self.actionexit.setObjectName("actionexit")
        self.actionexit.triggered.connect(self.exit)

        self.menu_file.addSeparator()
        self.menu_file.addSeparator()
        self.menu_file.addSeparator()
        self.menu_file.addSeparator()
        self.menu_file.addAction(self.actionnew_invoice)
        self.menu_file.addAction(self.actionopen_invoice)
        self.menu_file.addAction(self.actionsave)
        self.menu_file.addAction(self.actionexit)
        self.menubar.addAction(self.menu_file.menuAction())



        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)


    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))

        self.label_name.setText(_translate("MainWindow", "NAME"))
        self.label_address.setText(_translate("MainWindow", "ADDRESS"))
        self.label_byrsgst.setText(_translate("MainWindow", "BUYER'S GSTIN"))
        self.label_invoice.setText(_translate("MainWindow", "INVOICE NO"))
        self.label_challan.setText(_translate("MainWindow", "CHALLAN NO"))
        self.label_date.setText(_translate("MainWindow", "DATE"))

        item = self.tableWidget.horizontalHeaderItem(0)
        item.setText(_translate("MainWindow", "PRODUCT"))
        item = self.tableWidget.horizontalHeaderItem(1)
        item.setText(_translate("MainWindow", "HSN"))
        item = self.tableWidget.horizontalHeaderItem(2)
        item.setText(_translate("MainWindow", "QUANTITY"))
        item = self.tableWidget.horizontalHeaderItem(3)
        item.setText(_translate("MainWindow", "RATE"))
        item = self.tableWidget.horizontalHeaderItem(4)
        item.setText(_translate("MainWindow", "AMOUNT"))
        self.igst_checkbox.setText(_translate("MainWindow", "IGST"))
        self.gst12_checkbox.setText(_translate("MainWindow", "12% GST"))
        self.gst5_checkbox.setText(_translate("MainWindow", "5% GST"))



        self.save_Button.setText(_translate("MainWindow", "Save"))
        self.label_price.setText(_translate("MainWindow", "PRICE"))
        self.additem_Button.setText(_translate("MainWindow", "Add"))
        self.label_product.setText(_translate("MainWindow", "PRODUCT"))
        self.finalbill_Button.setText(_translate("MainWindow", "Finalize bill"))
        self.label_Quantity.setText(_translate("MainWindow", "QUANTITY"))
        self.print_Button.setText(_translate("MainWindow", "Print"))
        self.label_measure.setText(_translate("MainWindow", "MEASURE"))
        self.label_hsn.setText(_translate("MainWindow", "HSN"))
        self.menu_file.setTitle(_translate("MainWindow", "&file"))
        self.actionnew_invoice.setText(_translate("MainWindow", "new invoice"))
        self.actionopen_invoice.setText(_translate("MainWindow", "open invoice"))
        self.actionsave.setText(_translate("MainWindow", "save"))
        self.actionexit.setText(_translate("MainWindow", "exit"))


    def addNewPasses(self):
        self.save_Button.setEnabled(True)
        if(self.gst12_checkbox.isChecked()==True):
            self.addNewPasses12()
        elif(self.gst5_checkbox.isChecked()==True):
            self.addNewPasses5()
        else:
            self.addNewPasses18()

    def addNewPasses5(self):


        numRows = self.tableWidget.rowCount()
        for rows in range(5):
            self.tableWidget.insertRow(numRows)


        row=[]
        for i in range(0,numRows):
            v=self.tableWidget.item(i,4).text()
            row.append(v)
        bill_total=0
        for k in range(0,len(row)):
            try:
                bill_total+=float(row[k])
            except:
                self.messagebox("Error calculating Total")
        row1 = []
        newrow = []
        for i in range(0, numRows):
            v = self.tableWidget.item(i, 4).text()
            row1.append(v)
        for k in range(numRows):
            try:
                bill_total1 = float(row1[k])
                bill_total1 = 0.025 * bill_total1
                newrow.append(bill_total1)
            except:
                self.messagebox("Error calculating GST")


        print(newrow)
        part_gst=0
        for l in range(0,len(newrow)):
            part_gst+=newrow[l]


        try:
            total_gst = part_gst*2
            bill_gtotal = total_gst + bill_total
            number = float(bill_gtotal)
        except:
            self.messagebox("Error calculating GST")
        number1 = int(number)
        number_dec = float("%.1f" % float(str(number - int(number))[1:]))
        print(number_dec)
        if (number_dec > 0.5):
            number1 = number1 + 1
            bill_gtotal=number1
        else:
            bill_gtotal=number1

        self.tableWidget.setItem(numRows, 3, QtWidgets.QTableWidgetItem("Total"))
        self.tableWidget.setItem(numRows , 4, QtWidgets.QTableWidgetItem(str("%.2f"%bill_total)))
        if(self.igst_checkbox.isChecked()==False):
            self.tableWidget.setItem(numRows+1, 3, QtWidgets.QTableWidgetItem("SGST 2.5%"))
            self.tableWidget.setItem(numRows+1, 4, QtWidgets.QTableWidgetItem(str("%.2f"%part_gst)))
            self.tableWidget.setItem(numRows+2, 3, QtWidgets.QTableWidgetItem("CGST 2.5%"))
            self.tableWidget.setItem(numRows+2, 4, QtWidgets.QTableWidgetItem(str("%.2f"%part_gst)))
            self.tableWidget.setItem(numRows + 3, 3, QtWidgets.QTableWidgetItem("Grand Total"))
            self.tableWidget.setItem(numRows + 3, 4, QtWidgets.QTableWidgetItem(str("%.2f" % (bill_gtotal))))
        else:
            self.tableWidget.setItem(numRows+1, 3, QtWidgets.QTableWidgetItem("IGST 5%"))
            self.tableWidget.setItem(numRows+1, 4, QtWidgets.QTableWidgetItem(str("%.2f"%total_gst)))
            self.tableWidget.setItem(numRows + 2, 3, QtWidgets.QTableWidgetItem("Grand Total"))
            self.tableWidget.setItem(numRows + 2, 4, QtWidgets.QTableWidgetItem(str("%.2f" % (bill_gtotal))))

        self.finalbill_Button.setEnabled(False)
        self.additem_Button.setEnabled(False)
        self.print_Button.setEnabled(False)


    def addNewPasses18(self):

        numRows = self.tableWidget.rowCount()
        for rows in range(5):
            self.tableWidget.insertRow(numRows)


        row=[]
        for i in range(0,numRows):
            v=self.tableWidget.item(i,4).text()
            row.append(v)
        bill_total=0
        for k in range(0,len(row)):
            try:
                bill_total+=float(row[k])
            except:
                self.messagebox("Error calculating Total")
        row1 = []
        newrow = []
        for i in range(0, numRows):
            v = self.tableWidget.item(i, 4).text()
            row1.append(v)
        for k in range(numRows):
            try:
                bill_total1 = float(row1[k])
                bill_total1 = 0.09 * bill_total1
                newrow.append(bill_total1)
            except:
                self.messagebox("Error calculating GST")


        print(newrow)
        part_gst=0
        for l in range(0,len(newrow)):
            part_gst+=newrow[l]


        try:
            total_gst = part_gst*2
            bill_gtotal = total_gst + bill_total
            number = float(bill_gtotal)
        except:
            self.messagebox("Error calculating GST")
        number1 = int(number)
        number_dec = float("%.1f" % float(str(number - int(number))[1:]))
        print(number_dec)
        if (number_dec > 0.5):
            number1 = number1 + 1
            bill_gtotal=number1
        else:
            bill_gtotal=number1

        self.tableWidget.setItem(numRows, 3, QtWidgets.QTableWidgetItem("Total"))
        self.tableWidget.setItem(numRows , 4, QtWidgets.QTableWidgetItem(str("%.2f"%bill_total)))
        if(self.igst_checkbox.isChecked()==False):
            self.tableWidget.setItem(numRows+1, 3, QtWidgets.QTableWidgetItem("SGST 9%"))
            self.tableWidget.setItem(numRows+1, 4, QtWidgets.QTableWidgetItem(str("%.2f"%part_gst)))
            self.tableWidget.setItem(numRows+2, 3, QtWidgets.QTableWidgetItem("CGST 9%"))
            self.tableWidget.setItem(numRows+2, 4, QtWidgets.QTableWidgetItem(str("%.2f"%part_gst)))
            self.tableWidget.setItem(numRows + 3, 3, QtWidgets.QTableWidgetItem("Grand Total"))
            self.tableWidget.setItem(numRows + 3, 4, QtWidgets.QTableWidgetItem(str("%.2f" % (bill_gtotal))))
        else:
            self.tableWidget.setItem(numRows+1, 3, QtWidgets.QTableWidgetItem("IGST 18%"))
            self.tableWidget.setItem(numRows+1, 4, QtWidgets.QTableWidgetItem(str("%.2f"%total_gst)))
            self.tableWidget.setItem(numRows + 2, 3, QtWidgets.QTableWidgetItem("Grand Total"))
            self.tableWidget.setItem(numRows + 2, 4, QtWidgets.QTableWidgetItem(str("%.2f" % (bill_gtotal))))

        self.finalbill_Button.setEnabled(False)
        self.additem_Button.setEnabled(False)
        self.print_Button.setEnabled(False)

    def addNewPasses12(self):

        numRows = self.tableWidget.rowCount()
        for rows in range(5):
            self.tableWidget.insertRow(numRows)


        row=[]
        for i in range(0,numRows):
            v=self.tableWidget.item(i,4).text()
            row.append(v)
        bill_total=0
        for k in range(0,len(row)):
            try:
                bill_total+=float(row[k])
                print(bill_total)
            except:
                self.messagebox("Error calculating Total")
        row1 = []
        newrow = []
        for i in range(0, numRows):
            v = self.tableWidget.item(i, 4).text()
            row1.append(v)
        for k in range(numRows):
            try:
                bill_total1 = float(row1[k])
                bill_total1 = 0.06 * bill_total1
                newrow.append(bill_total1)
            except:
                self.messagebox("Error calculating GST")

        print(newrow)
        part_gst=0
        for l in range(0,len(newrow)):
            try:
                part_gst+=newrow[l]
            except:
                self.messagebox("enter correct value")


        try:
            total_gst = part_gst*2
            bill_gtotal = total_gst + bill_total
            number = float(bill_gtotal)
        except:
            self.messagebox("Error calculating GST")

        number1 = int(number)
        number_dec = float("%.1f" % float(str(number - int(number))[1:]))
        print(number_dec)
        if (number_dec > 0.5):
            number1 = number1 + 1
            bill_gtotal=number1
        else:
            bill_gtotal=number1

        self.tableWidget.setItem(numRows, 3, QtWidgets.QTableWidgetItem("Total"))
        self.tableWidget.setItem(numRows , 4, QtWidgets.QTableWidgetItem(str("%.2f"%bill_total)))
        if(self.igst_checkbox.isChecked()==False):
            self.tableWidget.setItem(numRows+1, 3, QtWidgets.QTableWidgetItem("SGST 6%"))
            self.tableWidget.setItem(numRows+1, 4, QtWidgets.QTableWidgetItem(str("%.2f"%part_gst)))
            self.tableWidget.setItem(numRows+2, 3, QtWidgets.QTableWidgetItem("CGST 6%"))
            self.tableWidget.setItem(numRows+2, 4, QtWidgets.QTableWidgetItem(str("%.2f"%part_gst)))
            self.tableWidget.setItem(numRows + 3, 3, QtWidgets.QTableWidgetItem("Grand Total"))
            self.tableWidget.setItem(numRows + 3, 4, QtWidgets.QTableWidgetItem(str("%.2f" % (bill_gtotal))))
        else:
            self.tableWidget.setItem(numRows+1, 3, QtWidgets.QTableWidgetItem("IGST 12%"))
            self.tableWidget.setItem(numRows+1, 4, QtWidgets.QTableWidgetItem(str("%.2f"%total_gst)))
            self.tableWidget.setItem(numRows + 2, 3, QtWidgets.QTableWidgetItem("Grand Total"))
            self.tableWidget.setItem(numRows + 2, 4, QtWidgets.QTableWidgetItem(str("%.2f" % (bill_gtotal))))

        self.finalbill_Button.setEnabled(False)
        self.additem_Button.setEnabled(False)
        self.print_Button.setEnabled(False)

    def addRow(self):
        # Retrieve text from QLineEdit
        measure=self.measure_textbox.text()
        product = self.product_textbox.text()
        hsn=self.hsn_textbox.text()
        quantity = self.quantity_textbox.text()
        price = self.price_textbox.text()
        ves=0
        if(len(product)== 0):
            self.messagebox("enter a product")
        elif(len(hsn)== 0):
            self.messagebox("Enter the Hsn")
        elif(len(quantity)== 0):
            self.messagebox("enter a qauntity")
        elif(len(measure)== 0):
            self.messagebox("enter a measure")
        elif(len(price)== 0):
            self.messagebox("enter a price")
        else:
            ves=1
        amount=0
        try:
            amount= float(quantity)*float(price)
            if (ves == 1):
                # Create a empty row at bottom of table
                numRows = self.tableWidget.rowCount()
                self.tableWidget.insertRow(numRows)
                # Add text to the row
                self.tableWidget.setItem(numRows, 0, QtWidgets.QTableWidgetItem(product))
                self.tableWidget.setItem(numRows, 1, QtWidgets.QTableWidgetItem(hsn))
                self.tableWidget.setItem(numRows, 2, QtWidgets.QTableWidgetItem(quantity + measure))
                self.tableWidget.setItem(numRows, 3, QtWidgets.QTableWidgetItem(str("%.2f" % float(price))))
                self.tableWidget.setItem(numRows, 4, QtWidgets.QTableWidgetItem(str("%.2f" % amount)))
                self.product_textbox.setText("")
                self.hsn_textbox.setText("")
                self.measure_textbox.setText("")
                self.quantity_textbox.setText("")
                self.price_textbox.setText("")
            else:
                self.messagebox("check the values")
                self.product_textbox.setText("")
                self.hsn_textbox.setText("")
                self.measure_textbox.setText("")
                self.quantity_textbox.setText("")
                self.price_textbox.setText("")

        except:
            self.messagebox("check for valid values")



    rows=dict()


    def file_save(self):
        numRows = self.tableWidget.rowCount()
        print(numRows)

        row = {'product': [], 'hsn': [], 'quantity': [], 'rate': [], 'amount': [], 'total': [], 'sgst': [], 'cgst': []
               ,'igst':[],'gtotal':[]}
        print(type(row))

        if (self.igst_checkbox.isChecked() == False):
            numr=5
        else:
            numr=5
        for i in range(numRows-numr):
            v = self.tableWidget.item(i, 0).text()
            row['product'].append(v)
            v1 = self.tableWidget.item(i, 1).text()
            row['hsn'].append(v1)
            v2 = self.tableWidget.item(i, 2).text()
            row['quantity'].append(v2)
            v3 = self.tableWidget.item(i, 3).text()
            row['rate'].append(v3)
            v4 = self.tableWidget.item(i, 4).text()
            row['amount'].append(v4)

        if(self.igst_checkbox.isChecked()==False):
            v5 = self.tableWidget.item(numRows - 5, 4).text()
            row['total'].append(v5)
            v6 = self.tableWidget.item(numRows - 4, 4).text()
            row['sgst'].append(v6)
            v7 = self.tableWidget.item(numRows - 3, 4).text()
            row['cgst'].append(v7)
            v9 = self.tableWidget.item(numRows -2, 4).text()
            row['gtotal'].append(v9)
        else:
            v5 = self.tableWidget.item(numRows - 5, 4).text()
            row['total'].append(v5)
            v8 = self.tableWidget.item(numRows - 4, 4).text()
            row['igst'].append(v8)
            v9 = self.tableWidget.item(numRows - 3, 4).text()
            row['gtotal'].append(v9)



        today = self.date_textbox.text()
        Bgst=self.byrsgst_gst_textbox.text()
        buyeraddress=self.address_textbox.text()
        invoice_no=self.invoice_textbox.text()
        challan_no=self.challan_textbox.text()
        company_name=self.name_textbox.text()

        verify = 0
        if (len(company_name) == 0):
            self.messagebox("enter a company name")
        elif (len(buyeraddress) == 0):
            self.messagebox("enter a Address")
        elif (len(Bgst) == 0):
            self.messagebox("enter a Buyers gst")
        elif (len(invoice_no) == 0):
            self.messagebox("enter a invoice no")
        elif (len(challan_no) == 0):
            self.messagebox("enter a challan")
        else:
            verify = 1

        if(verify==1):

            strTable = '<html>' \
                   '<style>'\
                   'table,th,td' \
                   '{' \
                   'height:33px;' \
                   'border-left:0.5px solid black;' \
                   'border-right:0.5px solid black;' \
                   'border-collapse:collapse;' \
                   '}' \
                   '#product{' \
                   'text-align:left;' \
                   '}' \
                   'table,th{' \
                   'border:0.5px solid black;' \
                   'border-radius:10px;' \
                   'text-align:center;' \
                   '}' \
                   '#totaltable{border:0.5px solid black;}' \
                   'body{margin-left:60px;}' \
                   '.invoicedetails{}' \
                   '#tablecust{width:603px;float:left;}' \
                   '#tablecust td{border:none;}' \
                   '#billinfo{width:237px;float:left;margin-left:10px;}' \
                   '#billinfo td{border:none;}' \
                   '.body{width:850;align:center}' \
                   '.finalbill{float:left;width:240px}' \
                   '.pattern{float:left;width:610px;height:190px;}' \
                   '#headerdetails{' \
                   'border:1px solid black;' \
                   'text-align:center;' \
                   'width:850px;' \
                   '}' \
                   'img{margin-top:25px;height:65px;width:65px;}' \
                   '.top{width:850px;height:100px;float:left;}' \
                   '.head{float:left;text-align:right;width:20%;}' \
                   '.heading{height:100px;color:#0C366B;float:left;text-align:center;width:70%;}' \
                   '</style>' \
                   '<div class="body">' \
                   '<body>' \
                   '<center>' \
                   '<h5>TAX INVOICE</h5>' \
                   '<div class="top">'\
                   '    <div class="head">'\
                   '        <img src="PC Logo.jpg">'\
                   '    </div>'\
                   '    <div class="heading">'\
                   '        <h1>PLUMBSTONE CORPORATION</h1>' \
                   '        <h4>DEALERS,PLUMBING & CIVIL CONTRACTOR</h4>'\
                   '    </div>'\
                   '</div>' \
                   '<table>' \
                   '<tr><td id="headerdetails"><h4>Dealers in:CPVC,UPVC,HDPE,G.I Pipes & fittings,Water Tank,Water Pumps &<br> All type of C.P Bathroom Fittings & Sanitary Ware</h4></td></tr></table>' \
                   '<br><table>' \
                   '<tr><td id="headerdetails"><h4>11,Shaukat Mansion,Aarey Road,Goregaon(west),Mumbai-400104.<br>Tel:022-28731460 Mob:+919699190874 E-mail:plumbstone59@gmail.com</h4></td></tr>' \
                   '</table>' \
                   '</center>' \
                   '<div>'\
                   '<br><div class="invoicedetails">' \
                   '<table id ="tablecust">' \
                   '<tr id="product"><td>'+"M/s:"+str(company_name)+'</td></tr><tr id="product"><td>'+str(buyeraddress)+'</td></tr><tr id="product"><td>'+"Buyer's GSTIN:"+str(Bgst)+'</td></tr></table><table id = "billinfo"><tr id="product"><td>'+"Invoice no:"+str(invoice_no)+'</td></tr><tr id="product"><td>'+"Challan no:"+str(challan_no)+'</td></tr><tr id="product"><td>'+"Date:"+str(today)+'</td></tr></table></div></div><br>'\
                   '<div class="invoicedetails">'\
                   '<table>' \
                   '<col width="50">' \
                   '<col width="400">' \
                   '<col width="90">' \
                   '<col width="90">' \
                   '<col width="120">' \
                   '<col width="120">' \
                   '<tr><th>Sr no</th>' \
                   '<th>Description</th>' \
                   '<th>HSN</th>' \
                   '<th>Qty</th>' \
                   '<th>Rate</th>' \
                   '<th>Amount</th>' \
                   '</tr>' \

            rowcount=numRows-5
            for i in range(0,rowcount):
                strRW = "<tr><td>" + str(i+1) + "</td><td id='product'>" + str(row['product'][i]) + "</td><td>" + str(row['hsn'][i]) + "</td><td>" + str(row['quantity'][i]) + "</td><td>" + str(row['rate'][i]) + "</td><td>" + str(row['amount'][i]) + "</td></tr>"
                strTable = strTable + strRW

            strRWEM=""
            for j in range(rowcount,17):
                strRWEM1 = "<tr><td>"  +"</td><td>" +  "</td><td>"  + "</td><td>"  + "</td><td>" + "</td><td>" + "</td></tr>"
                strRWEM+=strRWEM1
            strRW2 = "<div='finalbill'><table id='totaltable'><col width='120'><col width='120'><tr><td>" + "Total" + "</td><td>" + str(row['total'][0]) + "</td></tr>"



            strRW6 = "<tr id='totaltable'><td>" + "Grand total" + "</td><td>" + str(row['gtotal'][0]) + "</td></tr>"

            if(self.gst12_checkbox.isChecked()==False):
                if (self.igst_checkbox.isChecked() == False):
                    strRW3 = "<tr id='totaltable'><td>" + "SGST 9%" + "</td><td>" + str(row['sgst'][0]) + "</td></tr>"
                    strRW4 = "<tr id='totaltable'><td>" + "CGST 9%" + "</td><td>" + str(row['cgst'][0]) + "</td></tr>"
                    strRWPARTGST=strRW3+strRW4
                else:
                    strRW5 = "<tr id='totaltable'><td>" + "IGST 18%" + "</td><td>" + str(row['igst'][0]) + "</td></tr>"
                    strRWPARTGST=strRW5

                """CALULATING 12% TAX"""
            else:
                if (self.igst_checkbox.isChecked() == False):
                    strRW3 = "<tr id='totaltable'><td>" + "SGST 6%" + "</td><td>" + str(row['sgst'][0]) + "</td></tr>"
                    strRW4 = "<tr id='totaltable'><td>" + "CGST 6%" + "</td><td>" + str(row['cgst'][0]) + "</td></tr>"
                    strRWPARTGST=strRW3+strRW4
                else:
                    strRW5 = "<tr id='totaltable'><td>" + "IGST 12%" + "</td><td>" + str(row['igst'][0]) + "</td></tr>"
                    strRWPARTGST=strRW5

            strGST="<table><tr><td style='width:600px;border:1px solid black;text-align:left;'><h4>GSTIN:27AANPK8010D1ZW</td></tr><tr><td style='text-align:left;'><h5>Rupees in words:</h5>"+str(num2words(row['gtotal'][0]))+" only."+"</td></tr></table>"
            strRW7="</table></div><div class='pattern'>"+strGST+"<h6>We declare that this invioce shows the actual price of all goods described and all particulars are true and correct.</h6><h5>E & O.E</h5></div>"
            strTable = strTable+strRWEM+"</table></div><br>"+strRW7+strRW2+strRWPARTGST+strRW6+"<p style='float:left;margin-left:600px;'>for PLUMBSTONE CORPORATION</p></body></div></html>"

            hs = open("asciiCharHTMLTable.html", 'w')
            hs.write(strTable)
            hs.close()
            import pdfkit
            file_name=str(invoice_no)
            if(len(file_name)!=0):
                pdfkit.from_file('asciiCharHTMLTable.html', file_name+".pdf")
            else:
                self.messagebox("please enter invoice no")
        self.rows=row


    def printdoc(self):
        pass


    def new_invoice(self):
        self.name_textbox.setText("")
        self.address_textbox.setText("")
        self.byrsgst_gst_textbox.setText("")
        self.invoice_textbox.setText("")
        self.challan_textbox.setText("")
        self.product_textbox.setText("")
        self.price_textbox.setText("")
        self.hsn_textbox.setText("")
        self.price_textbox.setText("")
        self.quantity_textbox.setText("")
        self.measure_textbox.setText("")
        numrow=self.tableWidget.rowCount()
        nn=numrow
        self.finalbill_Button.setEnabled(True)
        self.additem_Button.setEnabled(True)
        self.print_Button.setEnabled(True)
        self.save_Button.setEnabled(False)
        try:
            self.rows['product'].clear()
            self.rows['hsn'].clear()
            self.rows['quantity'].clear()
            self.rows['rate'].clear()
            self.rows['amount'].clear()
            self.rows['total'].clear()
            self.rows['cgst'].clear()
            self.rows['sgst'].clear()
            self.rows['amount'].clear()

            print(self.rows)
        except:
            self.messagebox("please add rows")

        while(nn != 0):
            for i in range(0,numrow):
                self.tableWidget.removeRow(i)
                nn=self.tableWidget.rowCount()

    def messagebox(self,message):
        self.message=message
        msg = QtWidgets.QMessageBox()
        msg.setIcon(QtWidgets.QMessageBox.Warning)

        msg.setText(self.message)
        msg.setWindowTitle("Warning")
        msg.setStandardButtons(QtWidgets.QMessageBox.Ok)
        retval = msg.exec_()
        if retval == QtWidgets.QMessageBox.Ok:
            pass


    def exit(self):
        sys.exit()












import sys
app = QtWidgets.QApplication(sys.argv)
MainWindow = QtWidgets.QMainWindow()
ui = Ui_MainWindow()
ui.setupUi(MainWindow)
MainWindow.show()
sys.exit(app.exec_())
